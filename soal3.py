# Soal nomor 3
case = [[1,2], [3,4]], [[7,8,9], [6,5,4]]

for m in case:
    flat = []
    print("Matriks: ")
    for row in m : 
        print(row) 
    rez = [[m[j][i] for j in range(len(m))] for i in range(len(m[0]))] 
    print("Transpose: ") 
    for row in rez: 
        print(row) 
    print("Flatten: ")
    for row in rez:
        for r in row:
            flat.append(r)
    print(flat)
    print()
    