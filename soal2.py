# Soal nomor 2

case = ["{[()]}", "{[(])}", "{{[[(())]]}}", "{(([[{([[{}]])}]]))}", "{[{[(({[]})]]}]}"]
open_list = ["[","{","("] 
close_list = ["]","}",")"] 
c = 1
for myStr in case:
    stack = [] 
    for i in myStr: 
        if i in open_list: 
            stack.append(i) 
        elif i in close_list: 
            pos = close_list.index(i) 
            if ((len(stack) > 0) and (open_list[pos] == stack[len(stack)-1])): 
                stack.pop() 
            else: 
                result = "NO"
    if len(stack) == 0: 
        result = "YES"
    print("Case %d : %s (%s)" %(c,myStr,result))
    c = c+1
  